//
//  Filters.swift
//  swiftData
//
//  Created by Francisco Perez on 19/06/23.
//

import Foundation



enum NoteSortedBy: Identifiable,CaseIterable {
    var id: Self { self }
    
    case createdAt
    case content
    
    var text:String {
        switch self  {
        case .createdAt: return "Created at"
        case .content: return "Cotent"
        }
    }
}


enum OrderBy: Identifiable,CaseIterable {
    var id: Self { self }
    
    case ascending
    case descending
    
    var text:String {
        switch self {
        case .ascending:
            return "Ascending"
        case .descending:
            return "Descending"
        }
    }
}
