//
//  NoteListView.swift
//  swiftData
//
//  Created by Francisco Perez on 13/06/23.
//

import SwiftUI
import SwiftData

struct NoteListView: View {
    
    @Environment(\.modelContext) private var context
    @Query(sort: \.createdAt, order: .reverse) var allNotes: [Note]
    @Query(sort: \.name, order: .forward) var allTags: [Tag]
    @State var noteText = ""

    
    var body: some View {
        List {
            Section{
                
                DisclosureGroup("Create a note"){
                    TextField("Enter Text", text: $noteText,axis:.vertical)
                        .lineLimit(2...4)
                    
                    
                    DisclosureGroup("Tag with") {
                        if allTags.isEmpty {
                            Text("You dont have any tags yet. Please create one from Tags tab")
                                .foregroundStyle(Color.gray)
                        }
                        
                        ForEach(allTags) { tag in
                            
                            HStack {
                                Text(tag.name)
                                if tag.isChecked {
                                    Spacer()
                                    Image(systemName: "checkmark.circle")
                                        .symbolRenderingMode(.multicolor)
                                }
                            }
                            .frame(maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, alignment: .leading)
                            .contentShape(Rectangle())
                            .onTapGesture {
                                tag.isChecked.toggle()
                            }
                        }
                        
                    }
                    Button("Save") {
                        createNote()
                    }
                }
                
            }
            Section{
                if allNotes.isEmpty {
                    ContentUnavailableView("You dont have any notes yet", systemImage: "note")
                }
                else
                {
                    ForEach(allNotes) { note in
                        VStack(alignment: .leading) {
                            
                            Text(note.content)
                            if note.tags.count > 0 {
                                Text("Tags: " + note.tags.map{$0.name}.joined(separator:", "))
                                    .font(.caption)
                            }
                            
                            Text(note.createdAt,style: .time)
                                .font(.caption)
                        }
                    }
                    .onDelete{ IndexSet in
                        IndexSet.forEach { index in
                            context.delete(object: allNotes[index])
                            
                        }
                        try? context.save()
                        
                    }
                }
            }
        }
        
    }
    
    
    func createNote() {
        
        var tags = [Tag]()
        allTags.forEach { tag in
            if tag.isChecked {
                tags.append(tag)
                tag.isChecked = false
            }
            
        }
        
        
        let note = Note(id: UUID().uuidString, content: noteText, createdAt: .now,tags: tags)
        context.insert(note)
        try? context.save()
        noteText = ""
    }
    
}// end view



#Preview {
    NoteListView()
}
