//
//  TagsListView.swift
//  swiftData
//
//  Created by Francisco Perez on 19/06/23.
//

import SwiftUI
import SwiftData

struct TagsListView: View {
    
    @Environment(\.modelContext) private var context
    @Query(sort: \.name, order: .forward) var allTags: [Tag]
    @State var tagText = ""
    
    
    
    var body: some View {
        List {
            Section{
                
                DisclosureGroup("Create a Tag"){
                    TextField("Enter Text", text: $tagText,axis:.vertical)
                        .lineLimit(2...4)
                    
                    Button("Save") {
                        createTag()
                    }
                }
                
            }
            Section{
                if allTags.isEmpty {
                    ContentUnavailableView("You dont have any Tags yet", systemImage: "tag")
                }
                else
                {
                    ForEach(allTags) { tag in
                        
                        if tag.notes.count > 0 {
                            DisclosureGroup("\(tag.name) \(tag.notes.count)"){
                                ForEach(tag.notes) { note in
                                    Text(note.content)
                                }
                                .onDelete(perform: { indexSet in
                                    indexSet.forEach { index in
                                        context.delete(tag.notes[index])
                                    }
                                    try? context.save()
                                })
                            }
                            
                        }else{
                            Text(tag.name)
                        }
                        
                        
                    }
                    .onDelete{ IndexSet in
                        IndexSet.forEach { index in
                            context.delete(object: allTags[index])
                
                        }
                        try? context.save()
                        
                    }
                }
            }
        }
        
    }
    
    
    func createTag() {
        let tag = Tag(id: UUID().uuidString, name: tagText, notes: [])
        
        context.insert(tag)
        try? context.save()
        tagText = ""
    }
}

#Preview {
    TagsListView()
}
