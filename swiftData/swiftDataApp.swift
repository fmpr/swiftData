//
//  swiftDataApp.swift
//  swiftData
//
//  Created by Francisco Perez on 10/06/23.
//  updated
//

import SwiftData
import SwiftUI

@main
struct swiftDataApp: App {
    
    @State var noSearchText: String = ""
    @State var noteSortedBy = NoteSortedBy.createdAt
    @State var noteOrderBy = OrderBy.descending
    
    @State var tagSearchText = ""
    @State var tagOrderBy = OrderBy.ascending
    
    var body: some Scene {
        WindowGroup {
            TabView {
                noteList
                tagList
                
            }.modelContainer(for: [
                    Note.self,
                    Tag.self
                ])
        }
    }
    
    
    
    var noteList:some View {
        NavigationStack {
            NoteListView(allNotes: noteListQuery)
                .searchable(text: $noSearchText,prompt: "Search")
                .textInputAutocapitalization(.never)
                .navigationTitle("Notes")
                .toolbar {
                    ToolbarItem(placement: .topBarLeading) {
                        Menu {
                            Picker("Sort by ",selection: $noteSortedBy) {
                                ForEach(NoteSortedBy.allCases) {
                                    Text($0.text).id($0)
                                }
                            }
                        } label: {
                            Label(noteSortedBy.text, systemImage: "line.horizontal.3.decrease.circle")
                        }
                    }
                    
                    ToolbarItem(placement: .topBarTrailing) {
                        Menu {
                            Picker("Order by ",selection: $noteOrderBy) {
                                ForEach(OrderBy.allCases) {
                                    Text($0.text).id($0)
                                }
                            }
                        } label: {
                            Label(noteOrderBy.text, systemImage: "arrow.up.arrow.down")
                        }
                    }
                    
                    
                    
                }
        }
        .tabItem { Label("Notes",systemImage: "note") }
    }
    
    var noteListQuery: Query<Note,[Note]> {
        let sortOrder : SortOrder = noteOrderBy == .ascending ? .forward: .reverse
        var predicate: Predicate<Note>?
        if !noSearchText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            predicate = .init(#Predicate{$0.content.contains(noSearchText)})
        }
        if noteSortedBy == .content {
            return Query(filter: predicate,sort: \.content,order: sortOrder)
        }else
        {
            return Query(filter: predicate,sort: \.createdAt,order: sortOrder)
        }
    }
    
    var tagList: some View {
        NavigationStack {
            TagsListView()
                .navigationTitle("Tags")
        }
        .tabItem { Label("Tags",systemImage: "tag") }
        
    }
    
    
}
